This repository includes low-level unit-tests for MTE, PAC and BTI.
These tests are based on GTest framework and targeted to run on Android.
Every project is covered by a .bp file, that can be built and run independently.
