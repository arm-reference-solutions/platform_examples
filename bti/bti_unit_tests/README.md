# BTI Unit Tests

This tests suite focuses on testing Armv8.5-A Branch Target Identification feature
of the underlying hardware implementation.

## Test Matrix

According to `Table D5-26: Values taken by PSTATE.BTYPE on execution of an instruction`
in `Section D5.4.4: About PSTATE.BTYPE` in [1] these unit tests cover the following test matrix.
An instruction is always executed to target all the five possible landing pads:
`bti`, `bti c`, `bti j`, `bti jc` and `no landing pad`.

Instruction | PSTATE.BTYPE | Guarded Page | Non-Guarded Page
|--|:--:|:--:|:--:|:--:|
**Register is `x16` or `x17`** |
BR | `0b01` | BTI-1 | BTI-21
BRAA | `0b01` | BTI-2 | BTI-22
BRAAZ | `0b01` | BTI-3 | BTI-23
BRAB | `0b01` | BTI-4 | BTI-24
BRABZ | `0b01` | BTI-5 | BTI-25
|
**Not `x16` or `x17`** |
BR | `0b11 / 0b01` | BTI-16 | BTI-6
BRAA | `0b11 / 0b01` | BTI-17 | BTI-7
BRAAZ  | `0b11 / 0b01` | BTI-18 | BTI-8
BRAB | `0b11 / 0b01` | BTI-19 | BTI-9
BRABZ | `0b11 / 0b01` | BTI-20 | BTI-10
|
**Any register** |
BLR | `0b10` | BTI-11 | BTI-26
BLRAA | `0b10` | BTI-12 | BTI-27
BLRAAZ | `0b10` | BTI-13 | BTI-28
BLRAB | `0b10` | BTI-14 | BTI-29
BLRABZ | `0b10` | BTI-15 | BTI-30

## References

[1] [Arm® Architecture Reference Manual Armv8, for Armv8-A architecture profile, version fb](https://static.docs.arm.com/ddi0487/fb/DDI0487F_b_armv8_arm.pdf)
