// Copyright (c) 2020, Arm Ltd.
// All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>
#include <sys/auxv.h>
#include <sys/mman.h>
#include <sys/prctl.h>
#include <unistd.h>

#define PROT_FLAG_BTI (PROT_READ | PROT_EXEC | PROT_BTI)
#define PROT_FLAG (PROT_READ | PROT_EXEC)

#define SKIP_NOBTI()                                                           \
  do {                                                                         \
    if (!bti_enabled)                                                          \
      GTEST_SKIP();                                                            \
  } while (0 != 0)

#define SKIP_NOPAC()                                                           \
  do {                                                                         \
    if (!pac_enabled)                                                          \
      GTEST_SKIP();                                                            \
  } while (0 != 0)

#define ASM asm volatile

#define EXPECT_SIGILL(__what)                                                  \
  do {                                                                         \
    ASSERT_EXIT(__what, ::testing::KilledBySignal(SIGILL), "");                \
  } while (0 != 0)

static bool bti_enabled = 0;
static bool pac_enabled = 0;

static size_t alloc_size = 0;

extern "C" {
extern void *protected_page_start;
void hint_basic();
void hint_c();
void hint_j();
void hint_jc();
void hint_none();

extern void *guarded_functions_start;
void guarded_br(uintptr_t address, uint64_t reg);
void guarded_pacia_braa(uintptr_t address, uint64_t reg, uint64_t seed);
void guarded_pacia_braaz(uintptr_t address, uint64_t reg);
void guarded_pacib_brab(uintptr_t address, uint64_t reg, uint64_t seed);
void guarded_pacib_brabz(uintptr_t address, uint64_t reg);

void guarded_blr(uintptr_t address, uint64_t reg);
void guarded_pacia_blraa(uintptr_t address, uint64_t reg, uint64_t seed);
void guarded_paciza_blraaz(uintptr_t address, uint64_t reg);
void guarded_pacib_blrab(uintptr_t address, uint64_t reg, uint64_t seed);
void guarded_pacizb_blrabz(uintptr_t address, uint64_t reg);

void basic_function();
};

typedef void (*hint_fn_t)();

// Register parameters for guarded functions.
#define X10 2
#define X16 1
#define X17 0

// BR to __address with __reg.
// Returns to instruction after BR.
#define BR(__reg, __address)                                                   \
  ASM("mov  " __reg ", %0\n"                                                   \
      "adr  lr, #8\n"                                                          \
      "br   " __reg "\n"                                                       \
      "nop\n" ::"r"(__address)                                                 \
      : __reg, "lr")

// BLR to __address with __reg.
// Returns to instruction after BLR.
#define BLR(__reg, __address)                                                  \
  ASM("mov  " __reg ", %0\n"                                                   \
      "blr  " __reg "\n"                                                       \
      "nop\n" ::"r"(__address)                                                 \
      : __reg, "lr")

// Simulates returning to __address with BRAA __reg, __seed.
// Returns to instruction after BRAA.
#define PACIA_BRAA(__reg, __address, __seed)                                   \
  ASM("mov    " __reg ", %0\n"                                                 \
      "pacia  " __reg ", %1\n"                                                 \
      "adr    lr, #8\n"                                                        \
      "braa   " __reg ", %1\n"                                                 \
      "nop\n" ::"r"(__address),                                                \
      "r"(__seed)                                                              \
      : __reg, "lr")

// Simulates returning to __address with BLRAA __reg, __seed.
// Returns to instruction after BLRAA.
#define PACIA_BLRAA(__reg, __address, __seed)                                  \
  ASM("mov    " __reg ", %0\n"                                                 \
      "pacia  " __reg ", %1\n"                                                 \
      "blraa   " __reg ", %1\n"                                                \
      "nop\n" ::"r"(__address),                                                \
      "r"(__seed)                                                              \
      : __reg, "lr")

// Simulates returning to __address with BRAAZ __reg.
// Returns to instruction after BRAAZ.
#define PACIZA_BRAAZ(__reg, __address)                                         \
  ASM("mov    " __reg ", %0\n"                                                 \
      "paciza " __reg "\n"                                                     \
      "adr    lr, #8\n"                                                        \
      "braaz  " __reg "\n"                                                     \
      "nop\n" ::"r"(__address)                                                 \
      : __reg, "lr")

// Simulates returning to __address with BLRAAZ __reg.
// Returns to instruction after BLRAAZ.
#define PACIZA_BLRAAZ(__reg, __address)                                        \
  ASM("mov    " __reg ", %0\n"                                                 \
      "paciza " __reg "\n"                                                     \
      "blraaz  " __reg "\n"                                                    \
      "nop\n" ::"r"(__address)                                                 \
      : __reg, "lr")

// Simulates returning to __address with BRAB __reg, __seed.
// Returns to instruction after BRAB.
#define PACIB_BRAB(__reg, __address, __seed)                                   \
  ASM("mov    " __reg ", %0\n"                                                 \
      "pacib  " __reg ", %1\n"                                                 \
      "adr    lr, #8\n"                                                        \
      "brab   " __reg ", %1\n"                                                 \
      "nop\n" ::"r"(__address),                                                \
      "r"(__seed)                                                              \
      : __reg, "lr")

// Simulates returning to __address with BLRAB __reg, __seed.
// Returns to instruction after BLRAB.
#define PACIB_BLRAB(__reg, __address, __seed)                                  \
  ASM("mov    " __reg ", %0\n"                                                 \
      "pacib  " __reg ", %1\n"                                                 \
      "blrab   " __reg ", %1\n"                                                \
      "nop\n" ::"r"(__address),                                                \
      "r"(__seed)                                                              \
      : __reg, "lr")

// Simulates returning to __address with BRABZ __reg.
// Returns to instruction after BRABZ.
#define PACIZB_BRABZ(__reg, __address)                                         \
  ASM("mov    " __reg ", %0\n"                                                 \
      "pacizb " __reg "\n"                                                     \
      "adr    lr, #8\n"                                                        \
      "brabz  " __reg "\n"                                                     \
      "nop\n" ::"r"(__address)                                                 \
      : __reg, "lr")

// Simulates returning to __address with BLRABZ __reg.
// Returns to instruction after BLRABZ.
#define PACIZB_BLRABZ(__reg, __address)                                        \
  ASM("mov    " __reg ", %0\n"                                                 \
      "pacizb " __reg "\n"                                                     \
      "blrabz  " __reg "\n"                                                    \
      "nop\n" ::"r"(__address)                                                 \
      : __reg, "lr")

// -----------------
// PSTATE.BTYPE = 0b01: protected memory region, X16 or X17 register.
// -----------------

// [BTI-1]
// Note: such a tag references an entry in the test matrix.
// See README.md for details.
TEST(BR_Test, GuardedMemoryWithX16OrX17) {
  SKIP_NOBTI();

  ASSERT_EQ(mprotect((void *)&protected_page_start, alloc_size, PROT_FLAG_BTI),
            0);

  hint_fn_t should_pass[3] = {&hint_c, &hint_j, &hint_jc};
  hint_fn_t should_fail[2] = {&hint_basic, &hint_none};

  for (auto address : should_pass) {
    // [BTI-1]
    guarded_br((uintptr_t)address, X16);
    guarded_br((uintptr_t)address, X17);
  }

  for (auto address : should_fail) {
    // [BTI-1]
    EXPECT_SIGILL(guarded_br((uintptr_t)address, X16));
    EXPECT_SIGILL(guarded_br((uintptr_t)address, X17));
  }
}

// [BTI-2] [BTI-3]
TEST(BRAA_Test, GuardedMemoryWithX16OrX17) {
  SKIP_NOBTI();
  SKIP_NOPAC();

  ASSERT_EQ(mprotect((void *)&protected_page_start, alloc_size, PROT_FLAG_BTI),
            0);

  hint_fn_t should_pass[3] = {&hint_c, &hint_j, &hint_jc};
  hint_fn_t should_fail[2] = {&hint_basic, &hint_none};
  const uint64_t seed = 0x1234;

  for (auto address : should_pass) {
    // [BTI-2]
    guarded_pacia_braa((uintptr_t)address, X16, seed);
    guarded_pacia_braa((uintptr_t)address, X17, seed);
    // [BTI-3]
    guarded_pacia_braaz((uintptr_t)address, X16);
    guarded_pacia_braaz((uintptr_t)address, X17);
  }

  for (auto address : should_fail) {
    // [BTI-2]
    EXPECT_SIGILL(guarded_pacia_braa((uintptr_t)address, X16, seed));
    EXPECT_SIGILL(guarded_pacia_braa((uintptr_t)address, X17, seed));
    // [BTI-3]
    EXPECT_SIGILL(guarded_pacia_braaz((uintptr_t)address, X16));
    EXPECT_SIGILL(guarded_pacia_braaz((uintptr_t)address, X17));
  }
}

// [BTI-4] [BTI-5]
TEST(BRAB_Test, GuardedMemoryWithX16OrX17) {
  SKIP_NOBTI();
  SKIP_NOPAC();

  ASSERT_EQ(mprotect((void *)&protected_page_start, alloc_size, PROT_FLAG_BTI),
            0);

  hint_fn_t should_pass[3] = {&hint_c, &hint_j, &hint_jc};
  hint_fn_t should_fail[2] = {&hint_basic, &hint_none};
  const uint64_t seed = 0x1234;

  for (auto address : should_pass) {
    // [BTI-4]
    guarded_pacib_brab((uintptr_t)address, X16, seed);
    guarded_pacib_brab((uintptr_t)address, X17, seed);
    // [BTI-5]
    guarded_pacib_brabz((uintptr_t)address, X16);
    guarded_pacib_brabz((uintptr_t)address, X17);
  }

  for (auto address : should_fail) {
    // [BTI-4]
    EXPECT_SIGILL(guarded_pacib_brab((uintptr_t)address, X16, seed));
    EXPECT_SIGILL(guarded_pacib_brab((uintptr_t)address, X17, seed));
    // [BTI-5]
    EXPECT_SIGILL(guarded_pacib_brabz((uintptr_t)address, X16));
    EXPECT_SIGILL(guarded_pacib_brabz((uintptr_t)address, X17));
  }
}

// -----------------
// PSTATE.BTYPE = 0b01: non-protected memory region with any register.
// -----------------

// [BTI-6] [BTI-21]
TEST(BR_Test, NonGuardedMemoryAnyRegister) {
  ASSERT_EQ(mprotect((void *)&protected_page_start, alloc_size, PROT_FLAG), 0);

  hint_fn_t should_pass[5] = {&hint_c, &hint_j, &hint_jc, &hint_basic,
                              &hint_none};

  for (auto address : should_pass) {
    // [BTI-6]
    BR("x10", address);
    // [BTI-21]
    BR("x16", address);
    BR("x17", address);
  }
}

// [BTI-7] [BTI-8] [BTI-22] [BTI-23]
TEST(BRAA_Test, NonGuardedMemoryAnyRegister) {
  SKIP_NOPAC();

  ASSERT_EQ(mprotect((void *)&protected_page_start, alloc_size, PROT_FLAG), 0);

  hint_fn_t should_pass[5] = {&hint_c, &hint_j, &hint_jc, &hint_basic,
                              &hint_none};
  const uint64_t seed = 0x1234;

  for (auto address : should_pass) {
    // [BTI-7]
    PACIA_BRAA("x10", address, seed);
    // [BTI-8]
    PACIZA_BRAAZ("x10", address);
    // [BTI-22]
    PACIA_BRAA("x16", address, seed);
    PACIA_BRAA("x17", address, seed);
    // [BTI-23]
    PACIZA_BRAAZ("x16", address);
    PACIZA_BRAAZ("x17", address);
  }
}

// [BTI-9] [BTI-10] [BTI-24] [BTI-25]
TEST(BRAB_Test, NonGuardedMemoryAnyRegister) {
  SKIP_NOPAC();

  ASSERT_EQ(mprotect((void *)&protected_page_start, alloc_size, PROT_FLAG), 0);

  hint_fn_t should_pass[5] = {&hint_c, &hint_j, &hint_jc, &hint_basic,
                              &hint_none};
  const uint64_t seed = 0x1234;

  for (auto address : should_pass) {
    // [BTI-9]
    PACIB_BRAB("x10", address, seed);
    // [BTI-10]
    PACIZB_BRABZ("x10", address);
    // [BTI-24]
    PACIB_BRAB("x16", address, seed);
    PACIB_BRAB("x17", address, seed);
    // [BTI-25]
    PACIZB_BRABZ("x16", address);
    PACIZB_BRABZ("x17", address);
  }
}

// -----------------
// PSTATE.BTYPE = 0b11: with protected memory region.
// -----------------

// [BTI-16]
TEST(BR_Test, GuardedMemoryOtherRegisters) {
  SKIP_NOBTI();

  ASSERT_EQ(mprotect((void *)&protected_page_start, alloc_size, PROT_FLAG_BTI),
            0);

  hint_fn_t should_pass[2] = {&hint_j, &hint_jc};
  hint_fn_t should_fail[3] = {&hint_basic, &hint_c, &hint_none};

  register uintptr_t x0 __asm("x0") = 0;

  for (auto address : should_pass) {
    x0 = (uintptr_t)address;

    // [BTI-16]
    guarded_br(x0, X10);
  }

  for (auto address : should_fail) {
    x0 = (uintptr_t)address;

    // [BTI-16]
    EXPECT_SIGILL(guarded_br(x0, X10));
  }
}

// [BTI-17] [BTI-18]
TEST(BRAA_Test, GuardedMemoryOtherRegisters) {
  SKIP_NOBTI();
  SKIP_NOPAC();

  ASSERT_EQ(mprotect((void *)&protected_page_start, alloc_size, PROT_FLAG_BTI),
            0);

  hint_fn_t should_pass[2] = {&hint_j, &hint_jc};
  hint_fn_t should_fail[3] = {&hint_basic, &hint_c, &hint_none};
  const uint64_t seed = 0x1234;

  for (auto address : should_pass) {

    // [BTI-17]
    guarded_pacia_braa((uintptr_t)address, X10, seed);
    // [BTI-18]
    guarded_pacia_braaz((uintptr_t)address, X10);
  }

  for (auto address : should_fail) {
    // [BTI-17]
    EXPECT_SIGILL(guarded_pacia_braa((uintptr_t)address, X10, seed));
    // [BTI-18]
    EXPECT_SIGILL(guarded_pacia_braaz((uintptr_t)address, X10));
  }
}

// [BTI-19] [BTI-20]
TEST(BRAB_Test, GuardedMemoryOtherRegisters) {
  SKIP_NOBTI();
  SKIP_NOPAC();

  ASSERT_EQ(mprotect((void *)&protected_page_start, alloc_size, PROT_FLAG_BTI),
            0);

  hint_fn_t should_pass[2] = {&hint_j, &hint_jc};
  hint_fn_t should_fail[3] = {&hint_basic, &hint_c, &hint_none};
  const uint64_t seed = 0x1234;

  for (auto address : should_pass) {
    // [BTI-19]
    guarded_pacib_brab((uintptr_t)address, X10, seed);
    // [BTI-20]
    guarded_pacib_brabz((uintptr_t)address, X10);
  }

  for (auto address : should_fail) {
    // [BTI-19]
    EXPECT_SIGILL(guarded_pacib_brab((uintptr_t)address, X10, seed));
    // [BTI-20]
    EXPECT_SIGILL(guarded_pacib_brabz((uintptr_t)address, X10));
  }
}

// -----------------
// PSTATE.BTYPE = 0b10: protected memory region with any register.
// -----------------

// [BTI-11]
TEST(BLR_Test, GuardedMemoryAnyRegister) {
  SKIP_NOBTI();

  ASSERT_EQ(mprotect((void *)&protected_page_start, alloc_size, PROT_FLAG_BTI),
            0);

  hint_fn_t should_pass[2] = {&hint_c, &hint_jc};
  hint_fn_t should_fail[3] = {&hint_basic, &hint_j, &hint_none};

  for (auto address : should_pass) {
    // [BTI-11]
    guarded_blr((uintptr_t)address, X10);
    guarded_blr((uintptr_t)address, X16);
    guarded_blr((uintptr_t)address, X17);
  }

  for (auto address : should_fail) {
    // [BTI-11]
    EXPECT_SIGILL(guarded_blr((uintptr_t)address, X10));
    EXPECT_SIGILL(guarded_blr((uintptr_t)address, X16));
    EXPECT_SIGILL(guarded_blr((uintptr_t)address, X17));
  }
}

// [BTI-12] [BTI-13]
TEST(BLRAA_Test, GuardedMemoryAnyRegister) {
  SKIP_NOBTI();
  SKIP_NOPAC();

  ASSERT_EQ(mprotect((void *)&protected_page_start, alloc_size, PROT_FLAG_BTI),
            0);

  hint_fn_t should_pass[2] = {&hint_c, &hint_jc};
  hint_fn_t should_fail[3] = {&hint_basic, &hint_j, &hint_none};
  const uint64_t seed = 0x1234;

  for (auto address : should_pass) {
    // [BTI-12]
    guarded_pacia_blraa((uintptr_t)address, X10, seed);
    guarded_pacia_blraa((uintptr_t)address, X16, seed);
    guarded_pacia_blraa((uintptr_t)address, X17, seed);
    // [BTI-13]
    guarded_paciza_blraaz((uintptr_t)address, X10);
    guarded_paciza_blraaz((uintptr_t)address, X16);
    guarded_paciza_blraaz((uintptr_t)address, X17);
  }

  for (auto address : should_fail) {
    // [BTI-12]
    EXPECT_SIGILL(guarded_pacia_blraa((uintptr_t)address, X10, seed));
    EXPECT_SIGILL(guarded_pacia_blraa((uintptr_t)address, X16, seed));
    EXPECT_SIGILL(guarded_pacia_blraa((uintptr_t)address, X17, seed));
    // [BTI-13]
    EXPECT_SIGILL(guarded_paciza_blraaz((uintptr_t)address, X10));
    EXPECT_SIGILL(guarded_paciza_blraaz((uintptr_t)address, X16));
    EXPECT_SIGILL(guarded_paciza_blraaz((uintptr_t)address, X17));
  }
}

// [BTI-14] [BTI-15]
TEST(BLRAB_Test, GuardedMemoryAnyRegister) {
  SKIP_NOBTI();
  SKIP_NOPAC();

  ASSERT_EQ(mprotect((void *)&protected_page_start, alloc_size, PROT_FLAG_BTI),
            0);

  hint_fn_t should_pass[2] = {&hint_c, &hint_jc};
  hint_fn_t should_fail[3] = {&hint_basic, &hint_j, &hint_none};
  const uint64_t seed = 0x1234;

  for (auto address : should_pass) {
    // [BTI-14]
    guarded_pacib_blrab((uintptr_t)address, X10, seed);
    guarded_pacib_blrab((uintptr_t)address, X16, seed);
    guarded_pacib_blrab((uintptr_t)address, X17, seed);
    // [BTI-15]
    guarded_pacizb_blrabz((uintptr_t)address, X10);
    guarded_pacizb_blrabz((uintptr_t)address, X16);
    guarded_pacizb_blrabz((uintptr_t)address, X17);
  }

  for (auto address : should_fail) {
    // [BTI-14]
    EXPECT_SIGILL(guarded_pacib_blrab((uintptr_t)address, X10, seed));
    EXPECT_SIGILL(guarded_pacib_blrab((uintptr_t)address, X16, seed));
    EXPECT_SIGILL(guarded_pacib_blrab((uintptr_t)address, X17, seed));
    // [BTI-15]
    EXPECT_SIGILL(guarded_pacizb_blrabz((uintptr_t)address, X10));
    EXPECT_SIGILL(guarded_pacizb_blrabz((uintptr_t)address, X16));
    EXPECT_SIGILL(guarded_pacizb_blrabz((uintptr_t)address, X17));
  }
}

// -----------------
// PSTATE.BTYPE = 0b10: non-protected memory region with any register.
// -----------------

// [BTI-26]
TEST(BLR_Test, NonGuardedMemoryAnyRegister) {
  ASSERT_EQ(mprotect((void *)&protected_page_start, alloc_size, PROT_FLAG), 0);

  hint_fn_t should_pass[5] = {&hint_c, &hint_j, &hint_jc, &hint_basic,
                              &hint_none};

  for (auto address : should_pass) {
    // [BTI-26]
    BLR("x15", address);
    BLR("x16", address);
    BLR("x17", address);
  }
}

// [BTI-27] [BTI-28]
TEST(BLRAA_Test, NonGuardedMemoryAnyRegister) {
  SKIP_NOPAC();

  ASSERT_EQ(mprotect((void *)&protected_page_start, alloc_size, PROT_FLAG), 0);

  hint_fn_t should_pass[5] = {&hint_c, &hint_j, &hint_jc, &hint_basic,
                              &hint_none};
  const uint64_t seed = 0x1234;

  for (auto address : should_pass) {
    // [BTI-27]
    PACIA_BLRAA("x15", address, seed);
    PACIA_BLRAA("x16", address, seed);
    PACIA_BLRAA("x17", address, seed);
    // [BTI-28]
    PACIZA_BLRAAZ("x15", address);
    PACIZA_BLRAAZ("x16", address);
    PACIZA_BLRAAZ("x17", address);
  }
}

// [BTI-29] [BTI-30]
TEST(BLRAB_Test, NonGuardedMemoryAnyRegister) {
  SKIP_NOPAC();

  ASSERT_EQ(mprotect((void *)&protected_page_start, alloc_size, PROT_FLAG), 0);

  hint_fn_t should_pass[5] = {&hint_c, &hint_j, &hint_jc, &hint_basic,
                              &hint_none};
  const uint64_t seed = 0x1234;

  for (auto address : should_pass) {
    // [BTI-29]
    PACIB_BLRAB("x15", address, seed);
    PACIB_BLRAB("x16", address, seed);
    PACIB_BLRAB("x17", address, seed);
    // [BTI-30]
    PACIZB_BLRABZ("x15", address);
    PACIZB_BLRABZ("x16", address);
    PACIZB_BLRABZ("x17", address);
  }
}

TEST(BTI_LinkerTest, CallBasicFunction) {
  basic_function();
}

TEST(BTI_LinkerTest, BypassLandingPad) {
  // Function address set to its second instruction,
  // so the first executed instruction will not be a landing pad,
  // but an illegal instruction.
  EXPECT_SIGILL(((void (*)())((uint64_t)&basic_function + 4llu))());
}

int main(int argc, char **argv) {
  bti_enabled = getauxval(AT_HWCAP2) & HWCAP2_BTI;
  pac_enabled = getauxval(AT_HWCAP) & HWCAP_PACA;

  alloc_size = sysconf(_SC_PAGE_SIZE);

  if (mprotect((void *)&guarded_functions_start, alloc_size, PROT_FLAG_BTI) !=
      0) {
    return -1;
  }

  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
