// Copyright (c) 2020, Arm Ltd.
// All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <arm_acle.h>
#include <asm/hwcap.h>
#include <gtest/gtest.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/auxv.h>
#include <sys/mman.h>
#include <unistd.h>

#define MTE_GRANULE_SIZE 16
#define MTE_TAG_NUMBER 16
#define MTE_TAGGED_ADDRESS 56

#define PROT_FLAG (PROT_READ | PROT_WRITE | PROT_MTE)
#define MAP_FLAG (MAP_PRIVATE | MAP_ANONYMOUS)

#define MTE_GTEST_LOG 0
#if MTE_GTEST_LOG
#define LOG(...) printf(__VA_ARGS__)
#else
#define LOG(...)
#endif

#define ASM asm volatile

static size_t page_size = 0;

inline int64_t loadTagFromPointer(void *ptr) {
  return (int64_t)__arm_mte_get_tag(ptr) >> MTE_TAGGED_ADDRESS;
}

inline int64_t getTagFromPointer(void *ptr) {
  return (int64_t)ptr >> MTE_TAGGED_ADDRESS;
}

/* Test random tag generation:
   A pure, non-tagged pointer's tag is default zero.
   If we explicitly exclude zero from tag generation and tag another pointer,
   we should get different pointers, even though they point to the same location.
 */
TEST(MTETest, CreateRandomTag) {
  intptr_t *p0 = nullptr;

  // Create random tag for p1, excluding 0.
  intptr_t *p1 = __arm_mte_create_random_tag(p0, 1);

  LOG("p0 = 0x%16.16lx\n", (intptr_t)p0);
  LOG("p1 = 0x%16.16lx\n", (intptr_t)p1);

  // p1 should have a different tag than p0.
  ASSERT_NE(p0, p1);
}

/* Test tag increment:
   Incrementing a default (0) tag should result a pointer with tag equal to 1.
 */
TEST(MTETest, IncrementTag) {
  intptr_t *p0 = nullptr;
  intptr_t *p1 = __arm_mte_increment_tag(p0, 1);

  LOG("p0 = 0x%16.16lx\n", (intptr_t)p0);
  LOG("p1 = 0x%16.16lx\n", (intptr_t)p1);

  ASSERT_NE(p0, p1);
  ASSERT_EQ(getTagFromPointer(p1) - getTagFromPointer(p0), 1);
}

/* Test tag exclusion:
   We exclude tags by __arm_mte_exclude_tag one by one, until 1 is left.
   We expect none of the excluded tags are generated.
 */
TEST(MTETest, ExcludedTags) {
  int64_t *p0 = nullptr;
  int64_t excl0 = 0;

  int64_t *p1 = __arm_mte_create_random_tag(p0, excl0);
  int64_t excl1 = __arm_mte_exclude_tag(p0, excl0);

  ASSERT_NE(p0, p1);
  ASSERT_NE(excl0, excl1);

  // In each iteration we check if we get a different tag than the previously
  // excluded.
  for (int i = 0; i < MTE_TAG_NUMBER - 2; i++) {
    p0 = p1;
    excl0 = excl1;

    excl1 = __arm_mte_exclude_tag(p1, excl1);
    p1 = __arm_mte_create_random_tag(p0, excl1);

    ASSERT_NE(p0, p1);
    ASSERT_NE(excl0, excl1);
  }

  // As we've excluded all tags except one, any further tag creation should
  // result the same tag.
  for (int i = 0; i < MTE_TAG_NUMBER; i++) {
    p0 = __arm_mte_create_random_tag(p0, excl1);
    p1 = __arm_mte_create_random_tag(p0, excl1);
    ASSERT_EQ(p0, p1);
  }
}

/* Test pointer subtraction:
   Both pointers point to the same memory, but with different tags.
*/
TEST(MTETest, PointerSubtraction) {
  int64_t test = 0;
  int64_t *p0 = &test;

  int64_t excl = __arm_mte_exclude_tag(p0, 0);
  p0 = __arm_mte_create_random_tag(p0, excl);

  excl = __arm_mte_exclude_tag(p0, excl);
  int64_t *p1 = __arm_mte_create_random_tag(p0, excl);

  ASSERT_NE(p0, p1);

  int64_t diff = __arm_mte_ptrdiff(p0, p1);

  LOG("p0 = 0x%16.16lx\n", (intptr_t)p0);
  LOG("p1 = 0x%16.16lx\n", (intptr_t)p1);

  ASSERT_EQ(diff, 0);
}

/* Test for storing and loading tags from tag-memory.
   At the end of the test both pointers should have the same tag.
 */
TEST(MTETest, TagStoreAndLoad) {
  intptr_t *p0 = (intptr_t *)mmap(NULL, page_size, PROT_FLAG, MAP_FLAG, -1, 0);
  ASSERT_NE(p0, (void *)-1);

  int64_t excl = __arm_mte_exclude_tag(p0, 0);
  intptr_t *p1 = __arm_mte_create_random_tag(p0, excl);

  LOG("p0 = 0x%16.16lx\n", (intptr_t)p0);
  LOG("p1 = 0x%16.16lx\n", (intptr_t)p1);
  ASSERT_NE(p0, p1);

  // Store the tag of p1 and load it into p0.
  __arm_mte_set_tag(p1);
  p0 = __arm_mte_get_tag(p0);

  // Now the pointers should share the same tag.
  LOG("p0 = 0x%16.16lx\n", (intptr_t)p0);
  LOG("p1 = 0x%16.16lx\n", (intptr_t)p1);
  ASSERT_EQ(p0, p1);

  munmap(p0, page_size);
}

/* Test for storing and loading tags from tag-memory with DC GZVA instruction.
   At the end of the test both pointers should have the same tag.
 */
TEST(MTETest, DCGZVA) {
  intptr_t *p0 = (intptr_t *)mmap(NULL, page_size, PROT_FLAG, MAP_FLAG, -1, 0);
  ASSERT_NE(p0, (void *)-1);

  int64_t excl = __arm_mte_exclude_tag(p0, 0);
  intptr_t *p1 = __arm_mte_create_random_tag(p0, excl);

  LOG("p0 = 0x%16.16lx\n", (intptr_t)p0);
  LOG("p1 = 0x%16.16lx\n", (intptr_t)p1);
  ASSERT_NE(p0, p1);

  ASM("dc gzva, %0" ::"r"(p1) : "memory");
  p0 = __arm_mte_get_tag(p1);

  LOG("p0 = 0x%16.16lx\n", (intptr_t)p0);
  LOG("p1 = 0x%16.16lx\n", (intptr_t)p1);
  ASSERT_EQ(p0, p1);

  munmap(p0, page_size);
}

/* Test for storing and loading tags from tag-memory with DC GVA instruction.
   At the end of the test both pointers should have the same tag.
 */
TEST(MTETest, DCGVA) {
  intptr_t *p0 = (intptr_t *)mmap(NULL, page_size, PROT_FLAG, MAP_FLAG, -1, 0);
  ASSERT_NE(p0, (void *)-1);

  int64_t excl = __arm_mte_exclude_tag(p0, 0);
  intptr_t *p1 = __arm_mte_create_random_tag(p0, excl);

  LOG("p0 = 0x%16.16lx\n", (intptr_t)p0);
  LOG("p1 = 0x%16.16lx\n", (intptr_t)p1);
  ASSERT_NE(p0, p1);

  ASM("dc gva, %0" ::"r"(p1) : "memory");
  p0 = __arm_mte_get_tag(p1);

  LOG("p0 = 0x%16.16lx\n", (intptr_t)p0);
  LOG("p1 = 0x%16.16lx\n", (intptr_t)p1);
  ASSERT_EQ(p0, p1);

  munmap(p0, page_size);
}

/* Writing to a different tagged memory should cause segfault. */
TEST(MTETest, Segfault) {
  long *p0 = (long *)mmap(NULL, page_size, PROT_FLAG, MAP_FLAG, -1, 0);
  ASSERT_NE(p0, (void *)-1);

  // Set a new tag and store it in tag-memory.
  p0 = __arm_mte_create_random_tag(p0, 0);
  __arm_mte_set_tag(p0);

  // Write some data to memory.
  p0[0] = 1;
  p0[1] = 2;

  LOG("p0 + 0 = 0x%16.16lx\tp0[0] = %ld\n", (intptr_t)p0, p0[0]);
  LOG("p0 + 1 = 0x%16.16lx\tp0[1] = %ld\n", (intptr_t)(p0 + 1), p0[1]);

  // p0[2] should have a different tag than p0[0] and p0[1].
  LOG("p0[2] = 0x%16.16lx\n\n", (intptr_t)__arm_mte_get_tag(p0 + 2));
  ASSERT_EQ(loadTagFromPointer(p0), loadTagFromPointer(p0 + 1));
  ASSERT_NE(loadTagFromPointer(p0), loadTagFromPointer(p0 + 2));

  // Writing to the same memory should not be a problem.
  p0[0] = 3;
  p0[1] = 4;

  LOG("p0 + 0 = 0x%16.16lx\tp0[0] = %ld\n", (intptr_t)p0, p0[0]);
  LOG("p0 + 1 = 0x%16.16lx\tp0[1] = %ld\n", (intptr_t)(p0 + 1), p0[1]);

  // Writing to a different tagged memory should cause segfault.
  LOG("Expecting SIGSEGV:\t\tp0[2] = 5\n");
  ASSERT_EXIT(p0[2] = 5, ::testing::KilledBySignal(SIGSEGV), "");
}

/* Test user after free:
   After freeing the allocated memory, its pointer should have a different tag.
 */
TEST(MTETest, UseAfterFree) {
  long *p0 = (long *)malloc(sizeof(long));
  ASSERT_NE(p0, nullptr);

  *p0 = 1;

  int64_t oldTag = loadTagFromPointer(p0);

  LOG("p0 = 0x%16.16lx\t*p0 = %ld\n", (intptr_t)p0, *p0);

  // The tag should change after free.
  free(p0);

  LOG("p0 = 0x%16.16lx\tAfter free\n", (intptr_t)__arm_mte_get_tag(p0));
  EXPECT_NE(oldTag, loadTagFromPointer(p0));
}

/* Test copy-on-write with fork:
   Copy-on-write should work with forked processes if tagging is enabled.
 */
TEST(MTETest, CopyOnWrite) {
  intptr_t *p0 = (intptr_t *)mmap(NULL, page_size, PROT_FLAG, MAP_FLAG, -1, 0);
  ASSERT_NE(p0, nullptr);

  memset(p0, 0, page_size);

  int64_t excl = __arm_mte_exclude_tag(p0, 0);
  p0 = __arm_mte_create_random_tag(p0, excl);
  __arm_mte_set_tag(p0);

  int64_t tag = getTagFromPointer(p0);

  pid_t pid = fork();
  ASSERT_GE(pid, 0);

  if (pid == 0) {
    ASSERT_EQ(getTagFromPointer(p0), tag);

    p0 = __arm_mte_create_random_tag(p0, excl);
    __arm_mte_set_tag(p0);

    *p0 = 1;

    exit(0);
  }

  *p0 = 2;

  p0 = __arm_mte_create_random_tag(p0, excl);
  __arm_mte_set_tag(p0);

  *p0 = 3;

  wait(NULL);
  munmap(p0, page_size);
}

/* Test mkstemp:
   The tags should be cleared after mapping a temp file.
   This test needs root access, else fails.
 */
TEST(MTETest, mmapTempfile) {
  ASSERT_EQ(getuid(), (uid_t)0);

  char tempfileName[] = "/storage/XXXXXX";
  int fd = mkstemp(&tempfileName[0]);
  ASSERT_NE(fd, -1);

  for (size_t i = 0; i < page_size / sizeof(char); i++) {
    ASSERT_NE(write(fd, "0", sizeof(char)), -1);
  }

  char *mapped_file =
      (char *)mmap(NULL, page_size, PROT_FLAG, MAP_PRIVATE, fd, 0);
  ASSERT_NE(mapped_file, (void *)-1);

  // Tag the allocated page.
  int64_t excl = __arm_mte_exclude_tag(mapped_file, 0);
  mapped_file = __arm_mte_create_random_tag(mapped_file, excl);

  for (size_t i = 0; i < page_size - 1; i += MTE_GRANULE_SIZE) {
    __arm_mte_set_tag(mapped_file + i);
  }

  int64_t tag = getTagFromPointer((intptr_t *)mapped_file);

  // Check the tags.
  for (size_t i = 0; i < page_size - 1; i += MTE_GRANULE_SIZE) {
    ASSERT_EQ(loadTagFromPointer((intptr_t *)(mapped_file + i)), tag);
  }

  // Mapping the same file should clear its tags.
  munmap(mapped_file, page_size);
  mapped_file = (char *)mmap(NULL, page_size, PROT_FLAG, MAP_PRIVATE, fd, 0);
  ASSERT_NE(mapped_file, (void *)-1);

  for (size_t i = 0; i < page_size - 1; i += MTE_GRANULE_SIZE) {
    ASSERT_EQ(loadTagFromPointer((intptr_t *)(mapped_file + i)), 0);
  }

  munmap(mapped_file, page_size);
  close(fd);
}

/* Check if MTE is supported and enabled. */
TEST(MTETest, MTEIsEnabled) {
  register uint64_t x10 __asm("x10") = 0;

  ASM("mrs x10, ID_AA64PFR1_EL1" : "+r"(x10));

  uint64_t MTE = (x10 >> 8) & 0xF; // MTE 11-8

  ASSERT_EQ((getauxval(AT_HWCAP2) & HWCAP2_MTE) != 0, MTE != 0);
}

int main(int argc, char **argv) {
  page_size = sysconf(_SC_PAGE_SIZE);

  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
