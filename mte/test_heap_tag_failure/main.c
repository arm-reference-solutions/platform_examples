// Copyright (c) 2020, Arm Ltd.
// All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** Test heap tagging: allocate memory dynamically and overread it
 * Build:
 *   mmma -j40 vendor/arm/examples/mte && m -j20 userdataimage-nodeps && m -j40
 * Run:
 *   /data/nativetest64/test_heap_tag_fail/test_heap_tag_fail
 * Expect:
 *   Printed pointers with tags and a segmentation fault.
 */

#include <arm_acle.h>
#include <stdio.h>
#include <stdlib.h>


// Handling 4K pages is enough because it is the smallest page possible.
#define PAGE_ALIGN_MASK (~ (uintptr_t)(4 * 1024 - 1))


uintptr_t get_page_addr(void * ptr) {
    return (uintptr_t)ptr & PAGE_ALIGN_MASK;
}


int main(){
    char *ptr = NULL;

    // Make sure that we are not crossing to the next page.
    do {
        // NOTE: Do not care about free as the program will end shortly.
        ptr = malloc(5);
    } while (get_page_addr(ptr) != get_page_addr(ptr + 32));

    printf("malloc returned: %18p \n", ptr);

    for (int i = 0; i < 32 ; i++) {
        printf("Tag%02d: %18p \n", i , __arm_mte_get_tag(ptr + i));
    }

    printf("About to crash!\n");
    fflush(stdout);

    ptr[18] = 'a';
    printf("Char: %c \n", ptr[18]);

    return 0;
}
