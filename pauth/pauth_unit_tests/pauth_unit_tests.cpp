// Copyright (c) 2020, Arm Ltd.
// All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <elf.h>
#include <gtest/gtest.h>
#include <sys/auxv.h>
#include <sys/prctl.h>
#include <sys/ptrace.h>
#include <sys/uio.h>
#include <unistd.h>

#include <unwindstack/AndroidUnwinder.h>
#include <unwindstack/Error.h>
#include <unwindstack/Regs.h>
#include <unwindstack/RegsArm64.h>
#include <unwindstack/RegsGetLocal.h>
#include <unwindstack/UcontextArm64.h>
#include <unwindstack/Unwinder.h>

#include "pauth_unit_tests.h"

static bool pac_enabled = 0;
static bool fpac_implemented = 0;

using namespace unwindstack;

extern "C" {
void paciasp_retaa_test();
void pacibsp_retab_test();
void pacibsp_retaa_test();
void paciasp_retab_test();
void retaa_test();
void retab_test();
}

int ok(void) { return 0; }

static std::string GetBacktrace(AndroidUnwinder& unwinder, std::vector<FrameData>& frames) {
  std::string backtrace_str;
  for (auto& frame : frames) {
    backtrace_str += unwinder.FormatFrame(frame) + '\n';
  }
  return backtrace_str;
}

bool isFPACImplemented() {
  if (!(getauxval(AT_HWCAP) & HWCAP_CPUID)) {
    printf("Cannot emulate reading ID_AA64ISAR1_EL1 or ID_AA64ISAR2_EL1  with mrs.\n");
    return false;
  }

  uint64_t ID_AA64ISAR1_EL1 = 0;
  ASM("mrs %0, ID_AA64ISAR1_EL1" : "=r"(ID_AA64ISAR1_EL1));

  uint64_t ID_AA64ISAR2_EL1 = 0;
  ASM("mrs %0, ID_AA64ISAR2_EL1" : "=r"(ID_AA64ISAR2_EL1));

  // Reference: Arm ARM: AArch64 System Register Descriptions D13.2 General system control registers
  // FPAC is implemented when APA equals to 0b0100 or 0b0101.
  uint64_t APA = (ID_AA64ISAR1_EL1 >> 4) & 0xF;
  bool FPAC_APA = (APA == 4) || (APA == 5);
  // FPAC is implemented when APA3 equals to 0b0100 or 0b0101.
  uint64_t APA3 = (ID_AA64ISAR2_EL1 >> 12) & 0xF;
  bool FPAC_APA3 = (APA3 == 4) || (APA3 == 5);
  // FPAC is implemented when API equals to 0b0100 or 0b0101.
  uint64_t API = (ID_AA64ISAR1_EL1 >> 8) & 0xF;
  bool FPAC_API = (API == 4) || (API == 5);

  return (FPAC_APA ||  FPAC_APA3 || FPAC_API);
}

bool isPAC2Implemented() {
  if (!(getauxval(AT_HWCAP) & HWCAP_CPUID)) {
    printf("Cannot emulate reading ID_AA64ISAR1_EL1 or ID_AA64ISAR2_EL1  with mrs.\n");
    return false;
  }

  uint64_t ID_AA64ISAR1_EL1 = 0;
  ASM("mrs %0, ID_AA64ISAR1_EL1" : "=r"(ID_AA64ISAR1_EL1));

  uint64_t ID_AA64ISAR2_EL1 = 0;
  ASM("mrs %0, ID_AA64ISAR2_EL1" : "=r"(ID_AA64ISAR2_EL1));

  // Reference: Arm ARM: AArch64 System Register Descriptions D13.2 General system control registers
  // PAC2 is implemented when APA equals to 0b0011 or 0b0100 or 0b0101.
  uint64_t APA = (ID_AA64ISAR1_EL1 >> 4) & 0xF;
  bool PAC2_APA = (APA == 3) || (APA == 4) || (APA == 5);
  // PAC2 is implemented when APA3 equals to 0b0011 or 0b0100 or 0b0101.
  uint64_t APA3 = (ID_AA64ISAR2_EL1 >> 12) & 0xF;
  bool PAC2_APA3 = (APA3 == 3) || (APA3 == 4) || (APA3 == 5);
  // PAC2 is implemented when API equals to 0b0011 or 0b0100 or 0b0101.
  uint64_t API = (ID_AA64ISAR1_EL1 >> 8) & 0xF;
  bool PAC2_API = (API == 3) || (API == 4) || (API == 5);

  return (PAC2_APA || PAC2_APA3 || PAC2_API);
}

uint64_t getPACMask() {
  uint64_t base = 0xff7fffffffffffff;
  uint64_t result = base;

  XPACLRI(result);

  return ~result & base;
}

void signalHandler(__attribute__((unused)) int signal) {
  AndroidLocalUnwinder unwinder;
  AndroidUnwinderData data;
  ASSERT_TRUE(unwinder.Unwind(data));
  ASSERT_TRUE(unwinder.Unwind(std::nullopt, data));
  ASSERT_TRUE(unwinder.Unwind(getpid(), data));
  std::unique_ptr<Regs> regs(Regs::CreateFromLocal());
  RegsGetLocal(regs.get());

  arm64_ucontext_t* arm64_ucontext = reinterpret_cast<arm64_ucontext_t*>(malloc(sizeof(arm64_ucontext_t))); 
  memcpy(&arm64_ucontext->uc_mcontext.regs[0], regs->RawData(), ARM64_REG_LAST * sizeof(uint64_t));
  ASSERT_TRUE(arm64_ucontext != nullptr);
  ASSERT_TRUE(unwinder.Unwind(arm64_ucontext, data));
  free(arm64_ucontext);
  AndroidUnwinderData reg_data;
  ASSERT_TRUE(unwinder.Unwind(regs.get(), reg_data));
  ASSERT_EQ(data.frames.size(), reg_data.frames.size());
  // Make sure all of the frame data is exactly the same.
  for (size_t i = 0; i < data.frames.size(); i++) {
    SCOPED_TRACE("\nMismatch at Frame " + std::to_string(i) + "\nucontext trace:\n" +
                 GetBacktrace(unwinder, data.frames) + "\nregs trace:\n" +
                 GetBacktrace(unwinder, reg_data.frames));
    const auto& frame_context = data.frames[i];
    const auto& frame_reg = reg_data.frames[i];
    ASSERT_EQ(frame_context.num, frame_reg.num);
    ASSERT_EQ(frame_context.rel_pc, frame_reg.rel_pc);
    ASSERT_EQ(frame_context.pc, frame_reg.pc);
    ASSERT_EQ(frame_context.sp, frame_reg.sp);
    ASSERT_STREQ(frame_context.function_name.c_str(), frame_reg.function_name.c_str());
    ASSERT_EQ(frame_context.function_offset, frame_reg.function_offset);
    ASSERT_EQ(frame_context.map_info.get(), frame_reg.map_info.get());
  }

  return;
}

TEST(PAuthTest, Signing) {
  SKIP_NOPAC;

  uint64_t base = 0xdeadc0de;
  const uint64_t seed = 0x1234;

  uint64_t result = base;
  PACIASP(result);
  ASSERT_NE(base, result);

  result = base;
  PACIBSP(result);
  ASSERT_NE(base, result);

  result = base;
  PACIAZ(result);
  ASSERT_NE(base, result);

  result = base;
  PACIBZ(result);
  ASSERT_NE(base, result);

  result = base;
  PACIA1716(result, seed);
  ASSERT_NE(base, result);

  result = base;
  PACIB1716(result, seed);
  ASSERT_NE(base, result);

  result = base;
  PACIA(result, seed);
  ASSERT_NE(base, result);

  result = base;
  PACIB(result, seed);
  ASSERT_NE(base, result);

  result = base;
  PACIZA(result);
  ASSERT_NE(base, result);

  result = base;
  PACIZB(result);
  ASSERT_NE(base, result);
}

TEST(PAuthTestData, Signing) {
  SKIP_NOPAC;

  uint64_t base = 0xdeadc0de;
  const uint64_t seed = 0x1234;

  uint64_t result = base;
  PACDA(result, seed);
  ASSERT_NE(base, result);

  result = base;
  PACDB(result, seed);
  ASSERT_NE(base, result);

  result = base;
  PACDZA(result);
  ASSERT_NE(base, result);

  result = base;
  PACDZB(result);
  ASSERT_NE(base, result);
}

TEST(PAuthTest, Authentication) {
  SKIP_NOPAC;

  uint64_t base = 0xdeadc0de;
  const uint64_t seed = 0x1234;

  uint64_t result = base;

  EXPECT_NE_AND_FAULT_WITH_FPAC(AUTIASP(result));
  EXPECT_NE_AND_FAULT_WITH_FPAC(AUTIBSP(result));

  EXPECT_NE_AND_FAULT_WITH_FPAC(AUTIAZ(result));
  EXPECT_NE_AND_FAULT_WITH_FPAC(AUTIBZ(result));

  EXPECT_NE_AND_FAULT_WITH_FPAC(AUTIA1716(result, seed));
  EXPECT_NE_AND_FAULT_WITH_FPAC(AUTIB1716(result, seed));

  EXPECT_NE_AND_FAULT_WITH_FPAC(AUTIA(result, seed));
  EXPECT_NE_AND_FAULT_WITH_FPAC(AUTIB(result, seed));

  EXPECT_NE_AND_FAULT_WITH_FPAC(AUTIZA(result));
  EXPECT_NE_AND_FAULT_WITH_FPAC(AUTIZB(result));
}

TEST(PAuthTestData, Authentication) {
  SKIP_NOPAC;

  uint64_t base = 0xdeadc0de;
  const uint64_t seed = 0x1234;

  uint64_t result = base;

  EXPECT_NE_AND_FAULT_WITH_FPAC(AUTDA(result, seed));
  EXPECT_NE_AND_FAULT_WITH_FPAC(AUTDB(result, seed));

  EXPECT_NE_AND_FAULT_WITH_FPAC(AUTDZA(result));
  EXPECT_NE_AND_FAULT_WITH_FPAC(AUTDZB(result));
}

TEST(PAuthTest, Stripping) {
  SKIP_FPAC;
  SKIP_NOPAC;

  uint64_t base = 0xdeadc0de;

  uint64_t result = base;
  AUTIASP(result);
  XPACLRI(result);
  ASSERT_EQ(base, result);

  result = base;
  AUTIBSP(result);
  XPACLRI(result);
  ASSERT_EQ(base, result);

  result = base;
  AUTIZA(result);
  XPACI(result);
  ASSERT_EQ(base, result);

  result = base;
  AUTIZB(result);
  XPACI(result);
  ASSERT_EQ(base, result);
}

TEST(PAuthTest, Roundtrip) {
  SKIP_NOPAC;

  uint64_t base = 0xdeadc0de;
  const uint64_t seed = 0x1234;

  uint64_t result = base;
  PACIASP(result);
  AUTIASP(result);
  ASSERT_EQ(base, result);

  result = base;
  PACIBSP(result);
  AUTIBSP(result);
  ASSERT_EQ(base, result);

  result = base;
  PACIAZ(result);
  AUTIAZ(result);
  ASSERT_EQ(base, result);

  result = base;
  PACIBZ(result);
  AUTIBZ(result);
  ASSERT_EQ(base, result);

  result = base;
  PACIA1716(result, seed);
  AUTIA1716(result, seed);
  ASSERT_EQ(base, result);

  result = base;
  PACIB1716(result, seed);
  AUTIB1716(result, seed);
  ASSERT_EQ(base, result);

  result = base;
  PACIA(result, seed);
  AUTIA(result, seed);
  ASSERT_EQ(base, result);

  result = base;
  PACIB(result, seed);
  AUTIB(result, seed);
  ASSERT_EQ(base, result);

  result = base;
  PACIZA(result);
  AUTIZA(result);
  ASSERT_EQ(base, result);

  result = base;
  PACIZB(result);
  AUTIZB(result);
  ASSERT_EQ(base, result);
}

TEST(PAuthTestData, Roundtrip) {
  SKIP_NOPAC;

  uint64_t base = 0xdeadc0de;
  const uint64_t seed = 0x1234;

  uint64_t result = base;
  PACDA(result, seed);
  AUTDA(result, seed);
  ASSERT_EQ(base, result);

  result = base;
  PACDB(result, seed);
  AUTDB(result, seed);
  ASSERT_EQ(base, result);

  result = base;
  PACDZA(result);
  AUTDZA(result);
  ASSERT_EQ(base, result);

  result = base;
  PACDZB(result);
  AUTDZB(result);
  ASSERT_EQ(base, result);
}

TEST(PAuthTest, StrippingWithBuiltinReturnAddress) {
  SKIP_NOPAC;

  // __builtin_return_address strips PAC from pointer.
  uint64_t base = (uint64_t)__builtin_return_address(0);
  uint64_t result = base;

  XPACLRI(result);
  ASSERT_EQ(base, result);
}

TEST(PAuthTest, ExtractPAC) {
  SKIP_NOPAC;

  // Bit 55 should be zero in case of user space addresses.
  // Top byte is ignored.
  uint64_t base = 0xff7fffffffffffff;
  uint64_t result = base;

  // Calculate mask for PAC.
  XPACLRI(result);
  uint64_t instruction_mask = ~result & base;

  result = base;
  XPACI(result);
  ASSERT_EQ(base & ~(instruction_mask), result);

  result = base;
  XPACD(result);

  uint64_t data_mask = ~result & base;

  uint64_t base_instruction_address = 0xdeadc0de;
  PACIZA(base_instruction_address);
  uint64_t masked_instruction_address = base_instruction_address & ~(instruction_mask);

  uint64_t base_data_address = 0xdeadc0de;
  PACDZA(base_data_address);
  uint64_t masked_data_address = base_data_address & ~(data_mask);

  result = base_instruction_address;
  XPACLRI(result);
  ASSERT_EQ(masked_instruction_address, result);

  result = base_instruction_address;
  XPACI(result);
  ASSERT_EQ(masked_instruction_address, result);

  result = base_data_address;
  XPACD(result);
  ASSERT_EQ(masked_data_address, result);
}

TEST(PAuthTest, PACMask) {
  SKIP_NOPAC;

  uint64_t mask = getPACMask();

  pid_t pid = fork();
  ASSERT_GE(pid, 0);

  if (pid == 0) {
    ASSERT_NE(ptrace(PTRACE_TRACEME, pid, NULL, NULL), -1);
    raise(SIGSTOP);

    exit(0);
  } else {
    wait(NULL);

    struct user_pac_mask mask_regs;
    struct iovec io;
    io.iov_base = &mask_regs;
    io.iov_len = sizeof(struct user_pac_mask);

    ASSERT_NE(ptrace(PTRACE_GETREGSET, pid, NT_ARM_PAC_MASK,
                     reinterpret_cast<void *>(&io)),
              -1);
    ASSERT_EQ(mask, mask_regs.insn_mask);
  }
}

TEST(PAuthTest, KeyChange) {
  SKIP_NOPAC;

  int pipefd[2];
  pipe(pipefd);

  uint64_t base = 0x1234;

  pid_t pid = fork();
  ASSERT_GE(pid, 0);

  if (pid == 0) {
    prctl(PR_PAC_RESET_KEYS, 0, 0, 0, 0);

    uint64_t result = base;
    PACIAZ(result);

    write(pipefd[1], &result, sizeof(result));
    close(pipefd[1]);

    exit(0);
  }

  wait(NULL);

  uint64_t child;
  read(pipefd[0], &child, sizeof(child));
  close(pipefd[0]);

  uint64_t result = base;
  PACIAZ(result);
  ASSERT_NE(child, result);
}

TEST(PAuthTest, GenericAuthentication) {
  SKIP_NOPAC;

  register uint64_t REG(x10) = 0;
  register uint64_t REG(x11) = 0;
  register uint64_t REG(x12) = 0;

  ASM("pacga %0, %1, %2" : "+r"(x10) : "r"(x11), "r"(x12));
  ASSERT_EQ(x10 & 0x00000000FFFFFFFF, 0);

  uint64_t oldPAC = x10;

  x11 = 1;
  ASM("pacga %0, %1, %2" : "+r"(x10) : "r"(x11), "r"(x12));

  ASSERT_EQ(x10 & 0x00000000FFFFFFFF, 0);
  ASSERT_NE(x10, oldPAC);

  x11 = 0;
  ASM("pacga %0, %1, %2" : "+r"(x10) : "r"(x11), "r"(x12));

  ASSERT_EQ(x10 & 0x00000000FFFFFFFF, 0);
  ASSERT_EQ(x10, oldPAC);
}

TEST(PAuthTest, Unwind) {
  SKIP_NOPAC;

  signal(SIGUSR1, signalHandler);

  AndroidLocalUnwinder unwinder;
  AndroidUnwinderData data;
  ASSERT_TRUE(unwinder.Unwind(data));
  ASSERT_TRUE(unwinder.Unwind(std::nullopt, data));
  ASSERT_TRUE(unwinder.Unwind(getpid(), data));
  std::unique_ptr<Regs> regs(Regs::CreateFromLocal());

  arm64_ucontext_t* arm64_ucontext = reinterpret_cast<arm64_ucontext_t*>(malloc(sizeof(arm64_ucontext_t)));
  memcpy(&arm64_ucontext->uc_mcontext.regs[0], regs->RawData(), ARM64_REG_LAST * sizeof(uint64_t));
  ASSERT_TRUE(arm64_ucontext != nullptr);
  ASSERT_TRUE(unwinder.Unwind(arm64_ucontext, data));
  free(arm64_ucontext);
  AndroidUnwinderData reg_data;
  ASSERT_TRUE(unwinder.Unwind(regs.get(), reg_data));
  ASSERT_EQ(data.frames.size(), reg_data.frames.size());
  // Make sure all of the frame data is exactly the same.
  for (size_t i = 0; i < data.frames.size(); i++) {
    SCOPED_TRACE("\nMismatch at Frame " + std::to_string(i) + "\nucontext trace:\n" +
                 GetBacktrace(unwinder, data.frames) + "\nregs trace:\n" +
                 GetBacktrace(unwinder, reg_data.frames));
    const auto& frame_context = data.frames[i];
    const auto& frame_reg = reg_data.frames[i];
    ASSERT_EQ(frame_context.num, frame_reg.num);
    ASSERT_EQ(frame_context.rel_pc, frame_reg.rel_pc);
    ASSERT_EQ(frame_context.pc, frame_reg.pc);
    ASSERT_EQ(frame_context.sp, frame_reg.sp);
    ASSERT_STREQ(frame_context.function_name.c_str(), frame_reg.function_name.c_str());
    ASSERT_EQ(frame_context.function_offset, frame_reg.function_offset);
    ASSERT_EQ(frame_context.map_info.get(), frame_reg.map_info.get());
  }

  // Check the backtrace in signal handler too.
  kill(0, SIGUSR1);
}

TEST(PAuthTest, CheckReturnAddressSigned) {
  SKIP_NOPAC;

  // Check if the link register has the PAC or not.
  // If PAauth is enabled, the return address should contain PAC.
  register uint64_t REG(x11) = 0;
  ASM("mov x11, lr" : "+r"(x11)::);

  uint64_t base = x11;
  XPACI(x11);

  // x11 should hold the return address without PAC.
  ASSERT_NE(base, x11);
}

TEST(PAuthTest, AuthenticateThenReturn) {
  SKIP_NOPAC;

  paciasp_retaa_test();
  pacibsp_retab_test();

  if (isFPACImplemented()) {
    EXPECT_SIGILL(paciasp_retab_test());
    EXPECT_SIGILL(pacibsp_retaa_test());

    EXPECT_SIGILL(retab_test());
    EXPECT_SIGILL(retaa_test());
  } else {
    EXPECT_SIGSEGV(paciasp_retab_test());
    EXPECT_SIGSEGV(pacibsp_retaa_test());

    EXPECT_SIGSEGV(retab_test());
    EXPECT_SIGSEGV(retaa_test());
  }
}

TEST(PAuthDeathTest, SignFailure) {
  SKIP_NOPAC;

  const uint64_t seed = 0x1234;
  const uint64_t address = (uint64_t)&ok;

  uint64_t result = address;
  PACIASP(result);
  VALIDATE_ADDRESS(result);

  result = address;
  PACIBSP(result);
  VALIDATE_ADDRESS(result);

  result = address;
  PACIA1716(result, seed);
  VALIDATE_ADDRESS(result);

  result = address;
  PACIB1716(result, seed);
  VALIDATE_ADDRESS(result);

  result = address;
  PACIA(result, seed);
  VALIDATE_ADDRESS(result);

  result = address;
  PACIB(result, seed);
  VALIDATE_ADDRESS(result);

  result = address;
  PACIZA(result);
  VALIDATE_ADDRESS(result);

  result = address;
  PACIZB(result);
  VALIDATE_ADDRESS(result);
}

TEST(PAuthDeathTest, AuthFailure) {
  SKIP_NOPAC;

  const uint64_t seed = 0x1234;
  const uint64_t address = (uint64_t)&ok;

  uint64_t result = address;
  if (!fpac_implemented) {
    AUTIASP(result);
    VALIDATE_ADDRESS(result);
  } else {
    EXPECT_SIGILL(AUTIASP(result));
  }

  result = address;
  if (!fpac_implemented) {
    AUTIBSP(result);
    VALIDATE_ADDRESS(result);
  } else {
    EXPECT_SIGILL(AUTIBSP(result));
  }

  result = address;
  if (!fpac_implemented) {
    AUTIA1716(result, seed);
    VALIDATE_ADDRESS(result);
  } else {
    EXPECT_SIGILL(AUTIA1716(result, seed));
  }

  result = address;
  if (!fpac_implemented) {
    AUTIB1716(result, seed);
    VALIDATE_ADDRESS(result);
  } else {
    EXPECT_SIGILL(AUTIB1716(result, seed));
  }

  result = address;
  if (!fpac_implemented) {
    AUTIA(result, seed);
    VALIDATE_ADDRESS(result);
  } else {
    EXPECT_SIGILL(AUTIA(result, seed));
  }

  result = address;
  if (!fpac_implemented) {
    AUTIB(result, seed);
    VALIDATE_ADDRESS(result);
  } else {
    EXPECT_SIGILL(AUTIB(result, seed));
  }

  result = address;
  if (!fpac_implemented) {
    AUTIZA(result);
    VALIDATE_ADDRESS(result);
  } else {
    EXPECT_SIGILL(AUTIZA(result));
  }

  result = address;
  if (!fpac_implemented) {
    AUTIZB(result);
    VALIDATE_ADDRESS(result);
  } else {
    EXPECT_SIGILL(AUTIZB(result));
  }
}

TEST(PAuthTest, CheckHWCAP) {
  if (!(getauxval(AT_HWCAP) & HWCAP_CPUID)) {
    GTEST_SKIP();
  }

  uint64_t ID_AA64ISAR1_EL1 = 0;
  ASM("mrs %0, ID_AA64ISAR1_EL1" : "=r"(ID_AA64ISAR1_EL1));

  uint64_t ID_AA64ISAR2_EL1 = 0;
  ASM("mrs %0, ID_AA64ISAR2_EL1" : "=r"(ID_AA64ISAR2_EL1));

  uint64_t GPI = (ID_AA64ISAR1_EL1 >> 28) & 0xF; // GPI 31-28
  uint64_t GPA = (ID_AA64ISAR1_EL1 >> 24) & 0xF; // GPA 27-24
  uint64_t API = (ID_AA64ISAR1_EL1 >> 8) & 0xF;  // API 11-8
  uint64_t APA = (ID_AA64ISAR1_EL1 >> 4) & 0xF;  // APA 7-4
  uint64_t APA3 = (ID_AA64ISAR2_EL1 >> 12) & 0xF;  // APA3 15-12
  uint64_t GPA3 = (ID_AA64ISAR2_EL1 >> 8) & 0xF;   // GPA3 11-8

  ASSERT_EQ(pac_enabled, (GPI | GPA | GPA3) != 0 && (API | APA | APA3) != 0);
}

int main(int argc, char **argv) {
  pac_enabled = bool(getauxval(AT_HWCAP) & HWCAP_PACA);
  bool pac2_implemented = isPAC2Implemented();
  fpac_implemented = isFPACImplemented();

  printf("PAC is enabled by the kernel: %d\n", pac_enabled);
  printf("PAC2 is implemented by the hardware: %d\n", pac2_implemented);
  printf("FPAC is implemented by the hardware: %d\n", fpac_implemented);

  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
