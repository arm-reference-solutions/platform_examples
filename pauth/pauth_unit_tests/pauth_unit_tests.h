// Copyright (c) 2020, Arm Ltd.
// All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef PAUTH_UNIT_TESTS
#define PAUTH_UNIT_TESTS

#define ASM asm volatile

#define REG(__reg) __reg __asm(#__reg)

#define SKIP_NOPAC                                                             \
  do {                                                                         \
    if (!pac_enabled)                                                          \
      GTEST_SKIP();                                                            \
  } while (0 != 0)

#define SKIP_FPAC                                                              \
  do {                                                                         \
    if (fpac_implemented)                                                      \
      GTEST_SKIP();                                                            \
  } while (0 != 0)

#define VALIDATE_ADDRESS(__what)                                               \
  do {                                                                         \
    ASSERT_EXIT(((int (*)(void))__what)(), ::testing::KilledBySignal(SIGSEGV), \
                "");                                                           \
  } while (0 != 0)

#define EXPECT_SIGSEGV(__what)                                                 \
  do {                                                                         \
    ASSERT_EXIT(__what, ::testing::KilledBySignal(SIGSEGV), "");               \
  } while (0 != 0)

#define EXPECT_SIGILL(__what)                                                  \
  do {                                                                         \
    ASSERT_EXIT(__what, ::testing::KilledBySignal(SIGILL), "");                \
  } while (0 != 0)

#define PACIASP(__result)                                                      \
  ASM("mov x30, %0\n"                                                          \
      "paciasp\n"                                                              \
      "mov %0, x30\n"                                                          \
      : "+r"(__result)::"x30")

#define PACIBSP(__result)                                                      \
  ASM("mov x30, %0\n"                                                          \
      "pacibsp\n"                                                              \
      "mov %0, x30\n"                                                          \
      : "+r"(__result)::"x30")

#define PACIAZ(__result)                                                       \
  ASM("mov x30, %0\n"                                                          \
      "paciaz\n"                                                               \
      "mov %0, x30\n"                                                          \
      : "+r"(__result)::"x30")

#define PACIBZ(__result)                                                       \
  ASM("mov x30, %0\n"                                                          \
      "pacibz\n"                                                               \
      "mov %0, x30\n"                                                          \
      : "+r"(__result)::"x30")

#define PACIZA(__result)                                                       \
  ASM("mov x12, %0\n"                                                          \
      "paciza x12\n"                                                           \
      "mov %0, x12 \n"                                                         \
      : "+r"(__result)::"x12")

#define PACIZB(__result)                                                       \
  ASM("mov x12, %0\n"                                                          \
      "pacizb x12\n"                                                           \
      "mov %0, x12 \n"                                                         \
      : "+r"(__result)::"x12")

#define PACIA1716(__result, __seed)                                            \
  ASM("mov x17, %0\n"                                                          \
      "mov x16, %1\n"                                                          \
      "pacia1716 \n"                                                           \
      "mov %0, x17\n"                                                          \
      : "+r"(__result)                                                         \
      : "r"(__seed)                                                            \
      : "x16", "x17")

#define PACIB1716(__result, __seed)                                            \
  ASM("mov x17, %0\n"                                                          \
      "mov x16, %1\n"                                                          \
      "pacib1716 \n"                                                           \
      "mov %0, x17\n"                                                          \
      : "+r"(__result)                                                         \
      : "r"(__seed)                                                            \
      : "x16", "x17")

#define PACIA(__result, __seed)                                                \
  ASM("mov x10, %0\n"                                                          \
      "mov x11, %1\n"                                                          \
      "pacia x10, x11\n"                                                       \
      "mov %0, x10\n"                                                          \
      : "+r"(__result)                                                         \
      : "r"(__seed)                                                            \
      : "x10", "x11")

#define PACIB(__result, __seed)                                                \
  ASM("mov x10, %0\n"                                                          \
      "mov x11, %1\n"                                                          \
      "pacib  x10, x11\n"                                                      \
      "mov %0, x10\n"                                                          \
      : "+r"(__result)                                                         \
      : "r"(__seed)                                                            \
      : "x10", "x11")

#define AUTIASP(__result)                                                      \
  ASM("mov x30, %0\n"                                                          \
      "autiasp\n"                                                              \
      "mov %0, x30\n"                                                          \
      : "+r"(__result)::"x30")

#define AUTIBSP(__result)                                                      \
  ASM("mov x30, %0\n"                                                          \
      "autibsp\n"                                                              \
      "mov %0, x30\n"                                                          \
      : "+r"(__result)::"x30")

#define AUTIAZ(__result)                                                       \
  ASM("mov x30, %0\n"                                                          \
      "autiaz\n"                                                               \
      "mov %0, x30\n"                                                          \
      : "+r"(__result)::"x30")

#define AUTIBZ(__result)                                                       \
  ASM("mov x30, %0\n"                                                          \
      "autibz\n"                                                               \
      "mov %0, x30\n"                                                          \
      : "+r"(__result)::"x30")

#define AUTIZA(__result)                                                       \
  ASM("mov x12, %0\n"                                                          \
      "autiza x12\n"                                                           \
      "mov %0, x12\n"                                                          \
      : "+r"(__result)::"x12")

#define AUTIZB(__result)                                                       \
  ASM("mov x12, %0\n"                                                          \
      "autizb x12\n"                                                           \
      "mov %0, x12\n"                                                          \
      : "+r"(__result)::"x12")

#define AUTIA1716(__result, __seed)                                            \
  ASM("mov x17, %0\n"                                                          \
      "mov x16, %1\n"                                                          \
      "autia1716 \n"                                                           \
      "mov %0, x17\n"                                                          \
      : "+r"(__result)                                                         \
      : "r"(__seed)                                                            \
      : "x16", "x17")

#define AUTIB1716(__result, __seed)                                            \
  ASM("mov x17, %0\n"                                                          \
      "mov x16, %1\n"                                                          \
      "autib1716 \n"                                                           \
      "mov %0, x17\n"                                                          \
      : "+r"(__result)                                                         \
      : "r"(__seed)                                                            \
      : "x16", "x17")

#define AUTIA(__result, __seed)                                                \
  ASM("mov x10, %0\n"                                                          \
      "mov x11, %1\n"                                                          \
      "autia x10, x11\n"                                                       \
      "mov %0, x10\n"                                                          \
      : "+r"(__result)                                                         \
      : "r"(__seed)                                                            \
      : "x10", "x11")

#define AUTIB(__result, __seed)                                                \
  ASM("mov x10, %0\n"                                                          \
      "mov x11, %1\n"                                                          \
      "autib  x10, x11\n"                                                      \
      "mov %0, x10\n"                                                          \
      : "+r"(__result)                                                         \
      : "r"(__seed)                                                            \
      : "x10", "x11")

#define XPACLRI(__result)                                                      \
  ASM("mov x30, %0\n"                                                          \
      "xpaclri\n"                                                              \
      "mov %0, x30"                                                            \
      : "+r"(__result)::"x30")

#define XPACI(__result)                                                        \
  ASM("mov x10, %0\n"                                                          \
      "xpaci x10\n"                                                            \
      "mov %0, x10\n"                                                          \
      : "+r"(__result)::"x10")

#define XPACD(__result)                                                        \
  ASM("mov x10, %0\n"                                                          \
      "xpacd x10\n"                                                            \
      "mov %0, x10\n"                                                          \
      : "+r"(__result)::"x10")

#define PACDZA(__result)                                                       \
  ASM("mov x12, %0\n"                                                          \
      "pacdza x12\n"                                                           \
      "mov %0, x12 \n"                                                         \
      : "+r"(__result)::"x12")

#define PACDZB(__result)                                                       \
  ASM("mov x12, %0\n"                                                          \
      "pacdzb x12\n"                                                           \
      "mov %0, x12 \n"                                                         \
      : "+r"(__result)::"x12")

#define PACDA(__result, __seed)                                                \
  ASM("mov x10, %0\n"                                                          \
      "mov x11, %1\n"                                                          \
      "pacda x10, x11\n"                                                       \
      "mov %0, x10 \n"                                                         \
      : "+r"(__result)                                                         \
      : "r"(__seed)                                                            \
      : "x10", "x11")

#define PACDB(__result, __seed)                                                \
  ASM("mov x10, %0\n"                                                          \
      "mov x11, %1\n"                                                          \
      "pacdb  x10, x11\n"                                                      \
      "mov %0, x10 \n"                                                         \
      : "+r"(__result)                                                         \
      : "r"(__seed)                                                            \
      : "x10", "x11")

#define AUTDZA(__result)                                                       \
  ASM("mov x12, %0\n"                                                          \
      "autdza x12\n"                                                           \
      "mov %0, x12\n"                                                          \
      : "+r"(__result)::"x12")

#define AUTDZB(__result)                                                       \
  ASM("mov x12, %0\n"                                                          \
      "autdzb x12\n"                                                           \
      "mov %0, x12\n"                                                          \
      : "+r"(__result)::"x12")

#define AUTDA(__result, __seed)                                                \
  ASM("mov x10, %0\n"                                                          \
      "mov    x11, %1\n"                                                       \
      "autda  x10, x11\n"                                                      \
      "mov %0, x10\n"                                                          \
      : "+r"(__result)                                                         \
      : "r"(__seed)                                                            \
      : "x10", "x11")

#define AUTDB(__result, __seed)                                                \
  ASM("mov x10, %0\n"                                                          \
      "mov    x11, %1\n"                                                       \
      "autdb  x10, x11\n"                                                      \
      "mov %0, x10\n"                                                          \
      : "+r"(__result)                                                         \
      : "r"(__seed)                                                            \
      : "x10", "x11")

#define EXPECT_NE_AND_FAULT_WITH_FPAC(__what)                                  \
  result = base;                                                               \
  if (!fpac_implemented) {                                                     \
    __what;                                                                    \
    ASSERT_NE(base, result);                                                   \
  } else {                                                                     \
    EXPECT_SIGILL(__what);                                                      \
  }

#endif  // PAUTH_UNIT_TESTS
