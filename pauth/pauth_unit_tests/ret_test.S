// Copyright (c) 2020, Arm Ltd.
// All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

.section .note.gnu.property, "a"
    .balign 8
    .long 4
    .long 0x10
    .long 0x5
    .asciz "GNU"
    .long 0xc0000000 /* GNU_PROPERTY_AARCH64_FEATURE_1_AND */
    .long 4
    .long 3 /* PAuth and BTI */
    .long 0

.text

.p2align 12

.globl paciasp_retaa_test
.balign 2
.type paciasp_retaa_test, %function
paciasp_retaa_test:
    paciasp
    retaa

.globl pacibsp_retab_test
.balign 2
.type pacibsp_retab_test, %function
pacibsp_retab_test:
    pacibsp
    retab

# Functions below should cause segmentation fault.
.globl paciasp_retab_test
.balign 2
.type paciasp_retab_test, %function
paciasp_retab_test:
    paciasp
    retab

.globl pacibsp_retaa_test
.balign 2
.type pacibsp_retaa_test, %function
pacibsp_retaa_test:
    pacibsp
    retaa

.globl retaa_test
.balign 2
.type retaa_test, %function
retaa_test:
    hint #34 // bti c
    retaa

.globl retab_test
.balign 2
.type retab_test, %function
retab_test:
    hint #34 // bti c
    retab
